# Intro
This repo contains my own little package generator for AUR (ArchLinux User Repository).  
I created it because I need some package to be builderable on any architecture and I didn't want to manually enable it every time.  
So I created a handful of script there check if the orginal package have been updated. If...
- yes, the script will apply a patch/do some modification and commit and push the updated version to the AUR
- no, nothing is do

If you have a fix for something, merge request are welcome :D